package org.example.model;

public record Coordinates(double x, double y){}
