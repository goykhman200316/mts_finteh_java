package org.example.model.figure_behaviours;

public interface Scalable {
    void xScale(double scaleRate);
    void yScale(double scaleRate);
}
