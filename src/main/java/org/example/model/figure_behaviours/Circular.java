package org.example.model.figure_behaviours;

public interface Circular {
    double getSegmentArea(float angle);
    double getArcLength(float angle);
}
