package org.example.model.figure_behaviours;

import org.example.model.Coordinates;

public interface Moveable {
    void setCoordinatesTo(Coordinates newStartCoordinates);
    void moveOnVector(Coordinates vectorCoordinates);
}
