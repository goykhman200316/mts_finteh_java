package org.example.model.figure_behaviours;

import org.example.model.Coordinates;

import java.util.List;

public interface WithAngles{
    List<Coordinates> getVerticesCoordinates();
}
