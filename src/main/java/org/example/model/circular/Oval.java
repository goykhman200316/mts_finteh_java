package org.example.model.circular;

import org.example.model.Coordinates;
import org.example.model.GeomFigure;
import org.example.model.figure_behaviours.Circular;
import org.example.model.figure_behaviours.Scalable;

public  class Oval extends GeomFigure implements Circular, Scalable {
    private double big_half_axis;
    private double small_half_axis;
    private double center_x;
    private double center_y;

    public Oval(
            double big_half_axis,
            double small_half_axis,
            double center_x,
            double center_y
    ){
        super(center_x,center_y);
        this.center_x = center_x;
        this.center_y = center_y;
        this.big_half_axis = big_half_axis;
        this.small_half_axis = small_half_axis;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * (big_half_axis + small_half_axis);
    }

    @Override
    public double getArea() {
        return Math.PI * small_half_axis * big_half_axis;
    }

    @Override
    public void xScale(double scaleRate) {
        this.big_half_axis *= scaleRate;
    }

    @Override
    public void yScale(double scaleRate) {
        this.small_half_axis *= scaleRate;
    }
    @Override
    public void moveOnVector(Coordinates vectorCoordinates) {
        super.moveOnVector(vectorCoordinates);
        this.center_y+= vectorCoordinates.y();
        this.center_x+= vectorCoordinates.x();
    }

    @Override
    public void setCoordinatesTo(Coordinates newStartCoordinates) {
        super.setCoordinatesTo(newStartCoordinates);
        this.center_y = newStartCoordinates.y();
        this.center_x = newStartCoordinates.x();
    }

    @Override
    public String getFigureInfo() {
        return String.format(
                "Данная фигура - овал, его характеристики:" +
                        "\n точка пересечения осей(x = %g,y = %g) " +
                        "\n большая полуось - %g" +
                        "\n меньшая полуось - %g" +
                        "\n периметр - %g" +
                        "\n площадь- %g", center_x, center_y,big_half_axis,small_half_axis,getPerimeter(),getArea()
        );
    }

    @Override
    public double getSegmentArea(float angle) {
        //Приблизительно
        return getArea() * (angle/360);
    }

    @Override
    public double getArcLength(float angle) {
        //Приблизительно
        return getPerimeter() * (angle/360);
    }
}
