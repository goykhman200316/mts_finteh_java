package org.example.model.circular;

import org.example.model.Coordinates;

public class Circle extends Oval {
    private double radius;
    private double center_x;
    private double center_y;
    private String color;

    public String getColor() {
        return color;
    }

    public void changeRadius(double newRadius){
        this.radius = newRadius;
    }

    public Circle(
            double radius,
            double center_x,
            double center_y,
            String color
    ){
        super(
             radius, radius, center_x, center_y
        );
        this.radius = radius;
        this.center_x = center_x;
        this.center_y = center_y;
        this.color = color;
    }

    public Circle(
            double radius,
            double center_x,
            double center_y
    ){
        this(radius,center_x,center_y,"Black");
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }
    @Override
    public double getArea() {
        return Math.PI * Math.pow(radius,2);
    }

    @Override
    public double getSegmentArea(float angle) {
        return getArea() * (angle/360);
    }

    @Override
    public double getArcLength(float angle) {
        return getPerimeter() * (angle/360);
    }

    public void colorCircle(String newColor){
        this.color = newColor;
    }

    @Override
    public void moveOnVector(Coordinates vectorCoordinates) {
        super.moveOnVector(vectorCoordinates);
        this.center_y+= vectorCoordinates.y();
        this.center_x+= vectorCoordinates.x();
    }

    @Override
    public void setCoordinatesTo(Coordinates newStartCoordinates) {
        super.setCoordinatesTo(newStartCoordinates);
        this.center_y = newStartCoordinates.y();
        this.center_x = newStartCoordinates.x();
    }
    @Override
    public String getFigureInfo() {
        return String.format(
                "Данная фигура - круг, его характеристики:" +
                        "\nточка пересечения осей(x = %g,y = %g) " +
                        "\nрадиус - %g" +
                        "\nпериметр - %g" +
                        "\nплощадь- %g", this.center_x, this.center_y,this.radius,getPerimeter(),getArea()
        );
    }
}
