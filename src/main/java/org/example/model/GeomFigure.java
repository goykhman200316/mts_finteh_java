package org.example.model;

import org.example.model.figure_behaviours.Moveable;

public abstract class GeomFigure implements Moveable {
    protected double start_x;
    protected double start_y;
    protected GeomFigure(
            double start_x,
            double start_y
    ) {
        this.start_x = start_x;
        this.start_y = start_y;
    }

    @Override
    public void moveOnVector(Coordinates vectorCoordinates) {
        this.start_y+=vectorCoordinates.y();
        this.start_x+=vectorCoordinates.x();
    }

    @Override
    public void setCoordinatesTo(Coordinates newStartCoordinates) {
        this.start_x = newStartCoordinates.x();
        this.start_y = newStartCoordinates.y();
    }

    public abstract String getFigureInfo();
    public abstract double getPerimeter();
    public abstract double getArea();
}
