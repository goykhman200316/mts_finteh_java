package org.example.model.angular;

import org.example.model.Coordinates;
import org.example.model.GeomFigure;
import org.example.model.figure_behaviours.WithAngles;

import java.util.List;
import java.util.function.BiFunction;

public class Polygon extends GeomFigure implements WithAngles {
    private int vertices_count;
    private List<Coordinates> vertices_coordinates;

    private BiFunction<Coordinates,Coordinates,Double> liner = (Coordinates vertice1, Coordinates vertice2) -> {
        return vertice1.x() * vertice2.y();
    };
    private BiFunction<Coordinates,Coordinates,Double> side = (Coordinates vertice1, Coordinates vertice2) -> {
        return Math.sqrt(Math.pow(vertice1.x() - vertice2.x(), 2) + Math.pow(vertice1.y() - vertice2.y(), 2));
    };

    public Polygon(List<Coordinates> vertices_coordinates){
        this(new Coordinates(0,0), vertices_coordinates);
    }

    public Polygon(Coordinates startPoint, List<Coordinates> vertices_coordinates){
        super(startPoint.x(), startPoint.y());
        this.vertices_count = vertices_coordinates.size();
        this.vertices_coordinates = vertices_coordinates;
    }

    @Override
    public String getFigureInfo() {
        return String.format(
                "Данная фигура - многоугольник, его характеристики:" +
                        "\n стартовые координаты(x = %g,y = %g) " +
                        "\n кол-во вершин - %g", start_x,start_y, vertices_count
        );
    }

    @Override
    public double getPerimeter() {
        double result = 0;
        for (int i = 0; i < vertices_coordinates.size(); i++) {
            result += side.apply(
                    vertices_coordinates.get(i % vertices_count),
                    vertices_coordinates.get((i + 1) % vertices_count)
            );
        }
        return result;
    }

    @Override
    public double getArea() {
        double result = 0;
        for (int i = 0; i < vertices_coordinates.size(); i++) {
            result += liner.apply(
                    vertices_coordinates.get(i % vertices_count),
                    vertices_coordinates.get((i + 1) % vertices_count)
            ) - liner.apply(
                    vertices_coordinates.get((i+1)% vertices_count),
                    vertices_coordinates.get(i % vertices_count));
        }
        return result;
    }

    @Override
    public List<Coordinates> getVerticesCoordinates() {
        return vertices_coordinates;
    }

    protected void setVertices_coordinates(List<Coordinates> vertices_coordinates) {
        this.vertices_coordinates = vertices_coordinates;
    }
}
