package org.example.model.angular;

import org.example.model.Coordinates;

public class Square extends Rhombus {
    private double center_x;
    private double center_y;
    private double length_a;
    private String color;

    public Square(
            double length_a,
            double center_x,
            double center_y,
            String color
    ){
        super(
                center_x,
                center_y,
                length_a*Math.sqrt(2),
                length_a*Math.sqrt(2)
        );
        this.center_x = center_x;
        this.center_y = center_y;
        this.length_a = length_a;
        this.color = color;

    }

    public Square(
            double length_a,
            double center_x,
            double center_y
    ){
        this(length_a,center_x,center_y,"Black");
    }
    @Override
    public double getPerimeter() {
        return 4*length_a;
    }

    @Override
    public String getFigureInfo() {
        return String.format(
                "Данная фигура - квадрат, его характеристики:" +
                        "\n точка пересечения диагоналей(x = %g,y = %g) " +
                        "\n сторона - %g" +
                        "\n периметр - %g" +
                        "\n площадь- %g", this.center_x, this.center_y,this.length_a,getPerimeter(),getArea()
        );
    }

    @Override
    public double getArea() {
        return length_a*length_a;
    }

    @Override
    public void xScale(double scaleRate) {
        this.length_a *= scaleRate;
    }

    @Override
    public void yScale(double scaleRate) {
        this.length_a *= scaleRate;
    }

    public String getColor() {
        return color;
    }

    public void colorSquare(String newColor){
        this.color = newColor;
    }

    @Override
    public void moveOnVector(Coordinates vectorCoordinates) {
        super.moveOnVector(vectorCoordinates);
        this.center_y+= vectorCoordinates.y();
        this.center_x+= vectorCoordinates.x();
    }
    @Override
    public void setCoordinatesTo(Coordinates newStartCoordinates) {
        super.setCoordinatesTo(newStartCoordinates);
        this.center_y = newStartCoordinates.y();
        this.center_x = newStartCoordinates.x();
    }
}
