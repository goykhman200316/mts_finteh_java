package org.example.model.angular;

import org.example.model.Coordinates;
import org.example.model.figure_behaviours.Scalable;

import java.util.List;

public class Rhombus extends Polygon implements Scalable {

    private double center_x;
    private double center_y;
    private double small_diagonal;
    private double big_diagonal;

    public Rhombus(
            double center_x,
            double center_y,
            double small_diagonal,
            double big_diagonal
    ){
        super(
                new Coordinates(center_x,center_y),
                generateRhombusCoordinates(
                        center_x,
                        center_y,
                        small_diagonal,
                        big_diagonal
                )
        );
        this.center_x = center_x;
        this.center_y = center_y;
        this.big_diagonal = big_diagonal;
        this.small_diagonal = small_diagonal;
    }

    private List<Coordinates>  generateRhombusCoordinates(){
        return List.of(
                new Coordinates(this.center_x+this.big_diagonal/2, this.center_y),
                new Coordinates(this.center_x-this.big_diagonal/2, this.center_y),
                new Coordinates(this.center_x,this.center_y+this.small_diagonal/2),
                new Coordinates(this.center_x,this.center_y-this.small_diagonal/2)
        );
    }
    private static List<Coordinates>  generateRhombusCoordinates(
            double center_x,
            double center_y,
            double small_diagonal,
            double big_diagonal
    ){
        return List.of(
                new Coordinates(center_x+big_diagonal/2, center_y),
                new Coordinates(center_x-big_diagonal/2, center_y),
                new Coordinates(0,center_y+small_diagonal/2),
                new Coordinates(0,center_y-small_diagonal/2)
        );
    }

    @Override
    public void xScale(double scaleRate) {
        this.big_diagonal *= scaleRate;
        setVertices_coordinates(
                generateRhombusCoordinates()
        );
    }

    @Override
    public void yScale(double scaleRate) {
        this.small_diagonal *= scaleRate;
        setVertices_coordinates(
                generateRhombusCoordinates()
        );
    }

    @Override
    public double getArea() {
        return 0.5 * small_diagonal * big_diagonal;
    }

    @Override
    public double getPerimeter() {
        return 4 * Math.sqrt(Math.pow(0.5*small_diagonal,2) + Math.pow(0.5*big_diagonal,2));
    }

    @Override
    public String getFigureInfo() {
        return String.format(
                "Данная фигура - ромб, его характеристики:" +
                        "\n точка пересечения диагоналей(x = %g,y = %g) " +
                        "\n большая диагональ - %g" +
                        "\n меньшая диагональ - %g" +
                        "\n периметр - %g" +
                        "\n площадь- %g", center_x, center_y,small_diagonal,big_diagonal,getPerimeter(),getArea()
        );
    }

    @Override
    public void moveOnVector(Coordinates vectorCoordinates) {
        super.moveOnVector(vectorCoordinates);
        this.center_y+= vectorCoordinates.y();
        this.center_x+= vectorCoordinates.x();
        setVertices_coordinates(
                generateRhombusCoordinates()
        );
    }

    @Override
    public void setCoordinatesTo(Coordinates newStartCoordinates) {
        super.setCoordinatesTo(newStartCoordinates);
        this.center_y = newStartCoordinates.y();
        this.center_x = newStartCoordinates.x();
        setVertices_coordinates(
                generateRhombusCoordinates()
        );
    }
}
