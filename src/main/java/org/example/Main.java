package org.example;

import org.example.model.Coordinates;
import org.example.model.angular.Rhombus;
import org.example.model.angular.Square;
import org.example.model.circular.Circle;

public class Main {
    public static void main(String[] args) {
        secondTask();
    }
    /** <h1>Домашнее задание 2 - Основы ООП </h1>
     * <p>В данном домашнем задании реализованы различные группы фигур, которые демонстрируют принципы ООП в реализации их классов.
     * Данные фигуры делятся на 2 группы:
     * <ul>
     * <li>круговидные</li>
     * <li>угловатые</li>
     * </ul>
     *А также обладают свойствами:
     * <ul>
     * <li>растяжения</li>
     * <li>перемещения</li>
     * </ul>
     * <i><b>Вывод производится в консоль</b></i>
     * @author Геннадий Гойхман
     *</p>
     */
    private static void secondTask(){
        //1.1 Создайте экземпляры реализованных фигур,
        // поэкспериментируйте с вызовами методов и убедитесь, что все работает корректно.
        System.out.println("-------------------------------------------------------");
        Circle circle = new Circle(5, 4,7,"Pink");
        circle.colorCircle("Purple");
        Square square = new Square(8, 3,5,"Yellow");
        System.out.println(circle.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println(square.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println("Цвет круга: "+circle.getColor());
        System.out.println("Цвет квадрата: "+square.getColor());
        System.out.println("////////////////////////////////////////////////////");
        circle.moveOnVector(new Coordinates(564,82));
        square.moveOnVector(new Coordinates(85,34));
        square.colorSquare("Green");
        circle.changeRadius(8);
        System.out.println(circle.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println(square.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println("Немного ещё о круге:");
        System.out.printf("Площадь сегмента с углом 130 градусов: %g \n",  circle.getSegmentArea(130));
        System.out.printf("Длина дуги с углом 130 градусов: %g \n" ,  circle.getArcLength(130));

        System.out.println("***************************************************"+"\n");

        //1.2 Создайте объект ромба с любыми стартовыми значениями,
        // вызовите методы GetPerimeter, GetArea, а также ваши созданные методы
        // ,чтобы убедиться что ромб действительно правильно унаследовал все, что должен был.
        Rhombus rhombus = new Rhombus(27, 86, 15, 20);
        System.out.println(rhombus.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println("Площадь ромба - "+rhombus.getArea());
        System.out.println("Периметр ромба - "+rhombus.getPerimeter());
        System.out.println("////////////////////////////////////////////////////");
        rhombus.xScale(10);
        rhombus.yScale(3);
        rhombus.setCoordinatesTo(new Coordinates(15,15));
        System.out.println(rhombus.getFigureInfo());
        System.out.println("////////////////////////////////////////////////////");
        System.out.println("Координаты вершин ромба");
        rhombus.getVerticesCoordinates().forEach(coordinates ->{
            System.out.printf("- x= %g, y= %g%n",  coordinates.x(),coordinates.y());
        });
        System.out.println("-------------------------------------------------------");
    }
}
